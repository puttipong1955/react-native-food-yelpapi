import React, { useState, useEffect } from "react";
import yelp from "../api/yelp";

export default () => {
  const [results, setResults] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  const searchApi = async (searchTeam) => {
    try {
      const response = await yelp.get("/search", {
        params: { limit: 50, term: searchTeam, location: "NYC" },
      });
      setResults(response.data.businesses);
    } catch (error) {
      setErrorMessage("Some thing want wrong!");
    }
  };

  useEffect(() => {
    searchApi("pasta");
  }, []);

  return [searchApi, results, errorMessage];
};
